from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'djserver.views.home', name='home'),
                       # url(r'^djserver/', include('djserver.foo.urls')),

                       # Uncomment the admin/doc line below to enable admin documentation:
                       url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                       url(r'^see/', 'oper_server.views.see'),
                       url(r'^operation/', 'oper_server.views.operation'),
                       url(r'^torrent/', 'oper_server.views.update_torrent'),
                       url(r'^avp/', 'oper_server.views.avp'),
                       url(r'^hard/', 'oper_server.views.show_hard'),
                       url(r'^delete/(?P<comp_id>[0-9]+)', 'oper_server.views.delete_comp'),
                       url(r'^updatehard/','oper_server.views.update_hard_info'),
                       url(r'^updatehdd/','oper_server.views.check_update_hdd_info'),
                       url(r'^ideals/','oper_server.views.ideals'),
                       # Uncomment the next line to enable the admin:
                       #url(r'^admin/', include(admin.site.urls)),
)
