from django.db import models
from oper_server.models import Comp, Club
__author__ = 'jenko_2'


class DicGames(models.Model):
    dic_games_id = models.AutoField(primary_key=True)
    folder_name = models.CharField(max_length=255)
    size = models.IntegerField()
    accuracy = models.IntegerField()
    steam_id = models.IntegerField(unique=True)

    class Meta:
        db_table = 'dic_games'


class DicGamesComp(models.Model):
    dic_games_comp_id = models.IntegerField(primary_key=True)
    comp = models.ForeignKey(Comp, on_delete=models.CASCADE)
    dic_games = models.ForeignKey(DicGames, on_delete=models.CASCADE)
    size = models.IntegerField()
    mod_date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'dic_games_comp'


class DicGamesOffice(models.Model):
    dic_games_office_id = models.IntegerField(primary_key=True)
    dic_games = models.ForeignKey(DicGames, on_delete=models.CASCADE)
    club = models.ForeignKey(Club, on_delete=models.CASCADE)

    class Meta:
        db_table = 'dic_games_office'