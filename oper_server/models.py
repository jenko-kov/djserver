from django.db import models
from func import is_old_rec


class Club(models.Model):
    club_id = models.CharField(max_length=100, primary_key=True)
    club_ip = models.IPAddressField()
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.club_id


class Comp(models.Model):
    id = models.IntegerField(primary_key=True)
    club = models.ForeignKey(Club)
    num = models.IntegerField()
    freeC = models.IntegerField()
    freeD = models.IntegerField()
    mod_time = models.DateTimeField(auto_now=True)
    smart_status = models.TextField()
    cor_x = models.IntegerField()
    cor_y = models.IntegerField()
    exist = models.IntegerField()

    @property
    def bg_class(self):
        border_style = ''
        if self.freeC < 2000 or (self.freeD >= 0 and self.freeD < 10000) or self.old_on:
            res = 'comp bg_space_critical'
        elif self.freeC < 3000 or self.freeD >= 0 and self.freeD < 20000:
            res = 'comp bg_space_warning'
        elif 'Fail' in self.smart_status:
            res = 'comp bg_school'
        else:
            res = 'comp bg_resid'
        avp = Avp.objects.get(comp=self.pk)
        if avp.status == 0:
            border_style = ' b_avp_status'
        elif is_old_rec(avp.start):
            border_style = ' b_avp_time'
        return res + border_style

    @property
    def title(self):
        freeC = 'FreeC: {0} <br/>' if self.freeC > 3000 else '<strong style=\'color: #900;\'>FreeC: {0}<br/></strong>'
        freeD = 'FreeD: {1} <br/>' if self.freeD not in range(0, 20000) else '<strong style=\'color: #900;\'>FreeD: {1}<br/></strong>'
        mod_time = 'Time: {2} <br/>' if not self.old_on else '<strong style=\'color: #900;\'>Time: {2}<br/></strong>'
        smart = 'SMART: {3} <br/>' if 'Fail' not in self.smart_status else '<strong style=\'color: #900;\'>SMART: {3}</strong>'
        return (freeC + freeD + mod_time + smart + self.last_torrents_html + self.avp_html).format(self.freeC,
                                                                                                   self.freeD,
                                                                                                   self.mod_time.strftime('%d-%m-%Y %H:%M:%S'),
                                                                                                   self.smart_status)
    @property
    def last_torrents_html(self):
        torrents = Torrent.objects.filter(comp=self.pk).order_by('-ins_time')[0:3]
        str = ''
        if torrents:
            for tor in torrents:
                if tor.status in ['Seeding', 'Seeding[F]', 'Complete']:
                    color = '#009933'
                else:
                    color = '#900'
                str += '<br/><strong style=\'color: {3};\'>{0} {1}</strong> <br/>{2}'.format(tor.torrent_name,
                                                                                              tor.status,
                                                                                              tor.ins_time.strftime('%d-%m-%Y %H:%M:%S'),
                                                                                              color)
        return '<hr>' + str + '<hr>'

    @property
    def avp_html(self):
        rec = Avp.objects.get(comp=self.pk)
        status = 'OK' if rec.status == 1 else 'Fail'
        status_color = '#009933' if rec.status == 1 else '#900'
        time_color = '#000000' if not is_old_rec(rec.start) else '#900'
        html_str = '<strong style=\'color: {0};\'>AVP {1}</strong>' \
                   '<p style=\'color: {2};\'>Start: {3}<br/>End:  {4}</p>'.format(status_color,
                                                                                  status,
                                                                                  time_color,
                                                                                  rec.start,
                                                                                  rec.end)
        return html_str

    @property
    def old_on(self):
        return is_old_rec(self.mod_time)

    def __unicode__(self):
        return self.num


class Torrent(models.Model):
    comp = models.ForeignKey(Comp)
    ins_time = models.DateTimeField(auto_now=True)
    torrent_name = models.CharField(max_length=100)
    status = models.CharField(max_length=50)


class Avp(models.Model):
    comp = models.ForeignKey(Comp)
    start = models.DateTimeField()
    end = models.DateTimeField()
    status = models.IntegerField()


class HardInfo(models.Model):
    temp_video = models.CharField(max_length=32)
    temp_proc = models.CharField(max_length=32)
    img_ver = models.CharField(max_length=32)
    comp = models.ForeignKey(Comp, primary_key=True)
    VideoDriver = models.CharField(max_length=255)
    VideoAdapter = models.CharField(max_length=255)
    Time = models.DateTimeField(auto_now=True)
    SPD4 = models.CharField(max_length=255, default='<EMPTY>')
    SPD3 = models.CharField(max_length=255, default='<EMPTY>')
    SPD2 = models.CharField(max_length=255, default='<EMPTY>')
    SPD1 = models.CharField(max_length=255, default='<EMPTY>')
    SMART = models.CharField(max_length=255)
    RAM = models.CharField(max_length=255)
    Processor = models.CharField(max_length=255)
    OS = models.CharField(max_length=255)
    Motherboard = models.CharField(max_length=255)
    MAC = models.CharField(max_length=255)
    IP = models.CharField(max_length=255)
    IE = models.CharField(max_length=255)
    HDD = models.CharField(max_length=255)
    DiskD = models.CharField(max_length=255)
    DiskC = models.CharField(max_length=255)
    CompName = models.CharField(max_length=255)
    BIOS_Ver = models.CharField(max_length=255)

    class Meta:
        db_table = 'hard_space'