__author__ = 'jenko'
from django.utils import timezone
from datetime import timedelta


def get_table(comps, max_x, max_y):
    table = [[False for x in xrange(max_x)] for y in xrange(max_y)]
    for comp in comps:
        table[comp.cor_y][comp.cor_x] = comp
    return table


def is_old_rec(t):
    return timezone.now() - timedelta(1) > t