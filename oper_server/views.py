# Create your views here.
import json
from django.db.transaction import atomic
from django.http import HttpResponse
from oper_server.models import Club, Comp
from django.shortcuts import render
from update_info import update_data, update_tor, update_avp, update_hard, update_hdd
from oper_server.hdd.helper import IdealsProcessor
import datetime
import func


def see(request):
    comps = Comp.objects.all().order_by('club', 'num')
    return render(request, 'arch.html', {'comps': comps})


def operation(request):
    if request.method == 'POST':
        url = request.POST
        req = url["req"]
        res = update_data(json.loads(req))
        return HttpResponse(json.dumps(res))


def delete_comp(request, comp_id):
    qry = Comp.objects.filter(id=int(comp_id))
    if qry:
        qry.delete()
    return see(request)


def show_hard(request):
    comps = Comp.objects.all().order_by('club', 'num')
    max_x = 0
    max_y = 0
    for comp in comps:
        if comp.cor_x > max_x:
            max_x = comp.cor_x
        if comp.cor_y > max_y:
            max_y = comp.cor_y
    table = func.get_table(comps, max_x+1, max_y+1)
    return render(request, 'hard.html', {'table': table})


def create_workstation():
    sizes = {10: 97, 11: 52, 12: 80, 20: 89}
    Comp.objects.all().delete()
    for club, numbers in sizes.items():
        office = Club.objects.get(club_id=club)
        for num in xrange(numbers):
            comp = Comp()
            comp.num = num+1
            comp.club = office
            comp.freeC = 4000
            comp.freeD = 25000
            comp.mod_time = datetime.datetime.now()
            comp.smart_status = 'OK'
            comp.cor_x = club
            comp.cor_y = num
            comp.exist = 1
            comp.save()


def update_torrent(request):
    if request.method == 'POST':
        url = request.POST
        req = url["req"]
        res = update_tor(json.loads(req))
        return HttpResponse(json.dumps(res))


def avp(request):
    if request.method == 'POST':
        url = request.POST
        req = url["req"]
        res = update_avp(json.loads(req))
        return HttpResponse(json.dumps(res))


def update_hard_info(request):
    if request.method == 'POST':
        url = request.POST
        req = url["req"]
        res = update_hard(json.loads(req))
        return HttpResponse(json.dumps(res))


def check_update_hdd_info(request):
    if request.method == 'POST':
        url = request.POST
        req = url["req"]
        res = update_hdd(json.loads(req))
        return HttpResponse(json.dumps(res))


@atomic
def ideals(request):
    if request.method == 'POST':
        url = request.POST
        req = url["req"]
        proc = IdealsProcessor(json.loads(req))
        res = proc.process()
        return HttpResponse(json.dumps(res))