#coding=UTF-8
import win32serviceutil
import win32service
import win32event
import servicemanager
import socket
import os
import time
import win32api
import threading
import requests
import json

global _need_stop_
_need_stop_ = False


class AppServerSvc(win32serviceutil.ServiceFramework):
    _svc_name_ = "disk_check"
    _svc_display_name_ = "Free Space Check"
    _svc_description_ = "ITL service for monitoring free space."

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        socket.setdefaulttimeout(60)

    def SvcStop(self):
        global _need_stop_
        _need_stop_ = True
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE, servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_, ''))
        t = threading.Thread(target=self.main())
        t.daemon = True
        t.start()

    def main(self):
        ip = socket.gethostbyname(socket.gethostname())
        club = ip.split('.')[2]
        comp = ip.split('.')[3]
        t = time.time() - 3300  # додаємо, щоб в першу ітерацію пройшов запуск через 5 хв
        while not _need_stop_:
            time.sleep(5)
            if time.time() - t > 3600:
                try:
                    send_data(club, comp, get_free_space(r'C:'), get_free_space(r'D:'))
                except:
                    pass
                t = time.time()


def get_free_space(disk):
    if os.path.exists(disk):
        r = win32api.GetDiskFreeSpace(disk)
        free_space = r[0] * r[1] * r[2] / 1024 / 1024
    else:
        free_space = -1

    return free_space


def get_smart_status():
    res = os.popen("wmic diskdrive 0 get status")
    return res.read()[10:]


def send_data(club, comp, freeC, freeD):
    operation = {"club": club, "comp": comp, "freeC": freeC, "freeD": freeD, "smart_status": get_smart_status()}
    payload = {"req": json.dumps(operation)}

    requests.post('http://172.16.10.218:8000/operation/', payload)

if __name__ == '__main__':
    #--startup auto install
    win32serviceutil.HandleCommandLine(AppServerSvc)