$(document).ready(function(){
	map_update();
})

jQuery(function($){
	

	$(".comp").live('mouseenter',function(){
		if($(this).attr("title")!=""){
			var txt=$(this).attr("title");
			$(this).append("<div class='map_desc'>"+txt+"</div>");
			$(this).attr("title","");	
		}
	})
	
	$(".comp").live('mouseleave',function(){
		if($(this).children(".map_desc").length>0){
			$(this).attr("title",$(this).children(".map_desc").html());
			$(this).children(".map_desc").remove();	
		}
	})
	$(".map_desc").live("mouseenter",function(){
		$(this).parent().attr("title",$(this).html());
		$(this).remove();
	})

	var map_up=setInterval("map_update()",600000);


	$("#map td").live("click",function(){		/*КОНСТРУКТОР КАРТЫ*/
		if(!$(".move_sign").hasClass("active")){
			if(!$(this).find("span").hasClass("selected")){
				$("#map td.active").removeClass("active");
				$(this).addClass("active");
				if($(this).text()==""){
					var t = "";
				}else var t = parseInt($(this).text());
				var comp_num = t;			//ЕСЛИ УКАЗАНО - НОМЕР ПК
				var comp_ip = $(this).find("span").data("ip");			//ЕСЛИ УКАЗАНО - IP ПК
				var comp_mac = $(this).find("span").data("mac");			//ЕСЛИ УКАЗАНО - IP ПК
				var coord_top = $(this).offset().top + $(this).height();
				var coord_left = $(this).offset().left;
				console.log(comp_num);
				if(comp_num==""){
					$(".fd_del_pc, .fd_move_pc").css({"display":'none'});
				}else {
					$(".fd_del_pc, .fd_move_pc").css({"display":'inline-block'});
				}

				$("#add_pc").fadeOut("fast",function(){
					$(this).css({"left": coord_left, "top": coord_top}).fadeIn("fast");
					$("[name=pc_num]").val(comp_num);
					$("[name=pc_mac]").val(comp_mac);
					$("[name=pc_ip]").val(comp_ip);
				})
			}
		}else {
			if($(this).find("span").length==0){
				var lft = $(this).index();
				var top = $(this).parent().index();
				var id = $(".move_sign").data("id");
				var office_id = $("#club_id").val();
				$.ajax({
					type: "POST",
					url: base_url+"map/move_pc",
					async: true,
					data: {"id": id, "office_id": office_id, "left": lft, "top": top},
					success: function(data){
						if(data==1){
							$(".move_sign").removeClass('active');
							$(".move_sign").fadeOut("fast");
							$("#map td.active").removeClass("active");
							map_update();
						}else alert(data);
					}
				})
			}
		}
	})

	$(".fd_move_pc").live("click",function(){
		var id=0;
		id = $("#map td.active").find("span").data("id");
		$("#add_pc").fadeOut("fast");
		$('.move_sign').addClass('active');
		$(".move_sign").data({"id":id});
		$(".move_sign").fadeIn('fast');
	})

	$(".fd_cancel_move_pc").live("click",function(){
		$('.move_sign').removeClass('active');
		$(".move_sign").fadeOut('fast');
		$("#map td.active").removeClass("active");
	})

	$(".fd_cancel_add_pc").live("click",function(){		//ЗАКРЫТИЕ ОКНА ДОБАВЛЕНИЯ ПК
		$("#add_pc").fadeOut("fast");
		$("#map td.active").removeClass("active");
	})

	$(".fd_del_pc").live("click",function(){
		var id=0;
		id = $("#map td.active").find("span").data("id");
		var office_id = $("#club_id").val();
		if(id!=0){
			$.ajax({
				type: "POST",
				url: base_url+"map/del_pc",
				async: true,
				data: {"id": id, "office_id": office_id},
				success: function(data){
					if(data==1){
					$("#add_pc").fadeOut("fast");
					$("#map td.active").removeClass("active");
					map_update();
					}else alert(data);
				}
			})
		}else {
			$("#add_pc").fadeOut("fast");
			$("#map td.active").removeClass("active");
			map_update();
		}
	})

	$(".fd_add_pc").live("click",function(){				//ОТПРАВКА ЗАПРОСА НА ДОБАВЛЕНИЕ ПК
		var lft = $("#map td.active").index();
		var top = $("#map td.active").parent().index();
		var ip = $("[name=pc_ip]").val();
		var num = $("[name=pc_num]").val();
		var mac = $("[name=pc_mac]").val();
		var office_id = $("#club_id").val();
		var id = 0;
		if($("#map td.active span").length>0){		//РЕДАКТИРОВАНИЕ
			id = $("#map td.active").find("span").data("id");
		}

		$.ajax({
			type: "POST",
			url: base_url+"map/add_pc",
			async: true,
			data: {"left": lft, "top": top, "ip": ip, "mac": mac, "num": num, "id": id, "office_id": office_id},
			success: function(data){
				if(data==1){
					$("#add_pc").fadeOut("fast");
					$("#map td.active").removeClass("active");
					map_update();
				}else {
					$("#add_pc").find("div").append("<div class='warning'>"+data+"</div>");
						$(".warning").fadeIn("fast", function(){
							$(this).delay(3000).fadeOut('fast',function(){
							$(this).remove();
						})
					})
				}
			}
		})
	})

})

//СКРИПТ ОБНОВЛЕНИЯ ИНФОРМАЦИИ О КЛУБЕ
function map_update(){
	if($("#map td.active").length==0 && $(".comp.selected").length==0){
		$.ajax({
			url: base_url+"map/render_map",
			async: true,
			data: {"update": 1},
			type: "POST",
			success: function(response){
				/*$("#map").html(response);*/
				$.ajax({
					url: base_url+"map/get_stats",
					async: true,
					type: "POST",
					data: {"update": 1},
					success: function(response1){
						/*$("#stats").html(response1)*/

						/*$.ajax({
							url: base_url+"map/update_price",
							type: "POST",
							data:"",
							success: function(r){
								show(r);
							}
						})*/

					}
				})
			}
		})
	}
}